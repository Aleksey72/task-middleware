//Добавь функцию logRequestType как серверное middleware
//Ты должен изменить только server.js

import express from 'express';
import {logRequestType} from 'js/middleware.js';

const server = express();

server.use(logRequestType);

server.get('/', (req, res, next) => {
  res.send('Learning to use middleware!');
  next()
})
 
export { server };